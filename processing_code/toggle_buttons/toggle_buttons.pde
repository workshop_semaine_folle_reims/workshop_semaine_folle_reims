int nb = 16;
int x = 35;

int tempo = 80;
Pas pas [] = new Pas[nb];
Lecture lecture;

void setup() {
  size(600, 300);

  rectMode(CENTER);
  stroke(0);
  for (int i=0; i<nb; i++) {
    pas[i] = new Pas(30 + x*i);
  }
  lecture = new Lecture(30);
}

void draw() {
  background(175);
  for (int i=0; i<nb; i++) {
    pas[i].display(); 
    pas[i].update();
  }
  play();
}

void play() {
  lecture.display();
}

/*
*************
 ****************
 *******************
 ****************
 *************
 */

class Pas {
  int x;
  int y;
  int h = 30;
  int w = 30;
  boolean toggle = false;
  int  count = 0;

  Pas(int _x) {
    x = _x;
    y = height /2;
  }

  void display() {
    fill(255);
    rect(x, y, w, h);
  }

  void update() {
    if (mousePressed) {
      if (overRect(x, y, w, h) && count == 0) {
        toggle = !toggle;
        count++;
      }
    }
    else {
      count=0;
    }
    if (toggle) {
      line(x-w/2, y-h/2, x+w/2, y+h/2);
      line(x+w/2, y-h/2, x-w/2, y+h/2);
    }
  }

  boolean overRect(int _x, int _y, int _w, int _h) 
  {
    if (mouseX >= _x - _w/2 && mouseX <= _x + w/2 && 
      mouseY >= _y - _w/2 && mouseY <= _y + _w/2) {
      return true;
    } 
    else {
      return false;
    }
  }
}
/*
*************
 ****************
 *******************
 ****************
 *************
 */
class Lecture {
  float x;
  int y;
  int w = 10, h = 10;

  Lecture(int _x) {
    x = _x;
    y = height/2 - pas[0].h;
  }

  void display() {
    fill(7, 195, 245);
    for (int i = pas[0].x; i<pas[nb-1].x; i++) {
      x += i * 0.00002;
      if (x > pas[nb-1].x) x = pas[0].x;
      ellipse(x, y, w, h);
    }
  }
}

