// Example by Tom Igoe

import processing.serial.*;

// The serial port:
Serial myPort;       
int count = 0;

void setup() {
  // List all the available serial ports:
  println(Serial.list());

  /*  I know that the first port in the serial list on my mac
   is always my  Keyspan adaptor, so I open Serial.list()[0].
   Open whatever port is the one you're using.
   */
  myPort = new Serial(this, Serial.list()[0], 9600);
}

void draw() {

  keyboard();
  println(count);
}

void keyboard() {
  if (keyPressed) {
    switch(key) {
    case 'a':
      // Send a capital A out the serial port:
      if (count == 0)myPort.write('A');
      count++;
      //print("a  ");
      break;
    case 'z' :
      if (count == 0)myPort.write('Z');
      count++;
      print("z  ");
      break;
    case 'e' :
      if (count == 0)myPort.write('E');
      count++;
      print("e  ");
      break;
    }
  }
  else {
    count = 0;
  }
}

