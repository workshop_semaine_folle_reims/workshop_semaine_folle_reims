// mouseover serial

// Demonstrates how to send data to the Arduino I/O board, in order to
// turn ON a light if the mouse is over a square and turn it off
// if the mouse is not.

// created 2003-4
// based on examples by Casey Reas and Hernando Barragan
// modified 18 Jan 2009
// by Tom Igoe
// This example code is in the public domain.



import processing.serial.*;

float bx_1, bx_2, bx_3;
float by_1, by_2, by_3;
int count1, count2, count3 = 0;
int boxSize = 20;

Serial port;

void setup() {
  size(200, 200);
  background(0);
  bx_1 = 50;
  bx_2 = 100;
  bx_3 = 150;
  by_1 = by_2 = by_3 = height/2.0;
  rectMode(RADIUS);
  noStroke();
  // List all the available serial ports in the output pane.
  // You will need to choose the port that the Arduino board is
  // connected to from this list. The first port in the list is
  // port #0 and the third port in the list is port #2.
  println(Serial.list());

  // Open the port that the Arduino board is connected to (in this case #0)
  // Make sure to open the port at the same speed Arduino is using (9600bps)
  port = new Serial(this, Serial.list()[0], 9600);
}

void draw() {

  // Test if the cursor is over the box

  if (mouseX > bx_1-boxSize && mouseX < bx_1+boxSize &&
    mouseY > by_1-boxSize && mouseY < by_1+boxSize) {
    if (count1 == 0) {
      // draw a line around the box and change its color:
      background(219, 22, 187);
      // send an 'H' to indicate mouse is over square:
      port.write('A');
      count1++;
    }
  }
  else count1 = 0;
  fill(153);
  rect(bx_1, by_1, boxSize, boxSize);

  if (mouseX > bx_2-boxSize && mouseX < bx_2+boxSize &&
    mouseY > by_2-boxSize && mouseY < by_2+boxSize) {
    // draw a line around the box and change its color:
    if (count2 == 0) {
      background(245, 150, 7);
      // send an 'H' to indicate mouse is over square:
      port.write('Z');
      count2++;
    }
  }
  else count2 = 0;
  fill(53);
  rect(bx_2, by_2, boxSize, boxSize);


  if (mouseX > bx_3-boxSize && mouseX < bx_3+boxSize &&
    mouseY > by_3-boxSize && mouseY < by_3+boxSize) {
    // draw a line around the box and change its color:
    if (count3 == 0) { 
      background(46, 245, 7);
      // send an 'H' to indicate mouse is over square:
      port.write('E');
      count3++;
    }
  }
  else count3 = 0;

  // Draw the box

  fill(220);
  rect(bx_3, by_3, boxSize, boxSize);
}

