// chaine de 3 sequences rythmiques pour 3 solenoides
int incomingByte = 0;	// for incoming serial data

const int nb = 12;
int toy [nb];
char input [] = { 
  'A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'Q', 'S'}; 


void setup() {
  Serial.begin(9600);

  for(int i=0; i<nb; i++){
    toy[i] = i;
    pinMode(toy[i]+2, OUTPUT);
  }
}

void loop(){
  if(Serial.available() > 0){
    incomingByte = Serial.read();
    for(int i=0; i<nb; i++){
      if(incomingByte == input[i])
        digitalWrite(toy[i]+2, HIGH);
        delay(500);
    }
    
    delay(70);
    
    for(int i=0; i<nb; i++){
      digitalWrite(toy[i]+2, LOW);
      delay(300);
    }
  }
}




















