// chaine de 3 sequences rythmiques pour 3 solenoides
int incomingByte = 0;	// for incoming serial data

int brightness = 0;    // how bright the LED is
int fadeAmount = 1;    // how many points to fade the LED by

const int nb = 12;
int toy [nb];
char input [] = { 
  'A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'Q', 'S'}; 


void setup() {
  Serial.begin(9600);

  for(int i=0; i<nb; i++){
    toy[i] = i;
    pinMode(toy[i]+2, OUTPUT);
  }

  //vactrol
  pinMode(9, OUTPUT);
}

void loop(){
  if(Serial.available() > 0){
    incomingByte = Serial.read();
    // say what you got:
    //Serial.print("I received: ");
    //Serial.println(incomingByte, DEC);
    for(int i=0; i<nb; i++){
      if(incomingByte == input[i])
        digitalWrite(toy[i]+2, HIGH);
    }

    delay(70);

    for(int i=0; i<nb; i++){
      digitalWrite(toy[i]+2, LOW);
    }
  }
  
  // set the brightness of pin 9:
  analogWrite(9, brightness);    

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade: 
  if (brightness == 0 || brightness == 255) {
    fadeAmount = -fadeAmount ; 
  }     
  // wait for 30 milliseconds to see the dimming effect    
  delay(30); 
}





















