// chaine de 3 sequences rythmiques pour 3 solenoides

const int ledPin = 13; // Numéro du port pour la  LED
const int toy1 = 3;

//const int toy4 = 6;
int tempo = 80;
int pas = 0;
int maxpas = 16;
int sol1[] [16]= {
  { 
    1,0,0,1,1,1,0,0,1,0,0,0,0,0,0,0       }
  , // partition du solenoide 1 sequ 1
  { 
    1,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0       }
  , // partition du solenoide 1 sequ 2
  { 
    0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1       }  // partition du solenoide 1 sequ 3
};

int seq = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(toy1, OUTPUT);

}

void loop(){
  // chainage des sequences
  // numéro de séquence et nombre de répétitions de chaque séquence
  sequence(0, 10);
//  sequence(1, 3);
 // sequence(2, 5);
  // delay (5000);
  while(1); // tant que 1 = 1 boucle infinie = arreter à la fin
}

void sequence(int numseq, int repet) { // numéro de séquence et nombre de répétitions de chaque séquence
  for (int i = 0; i < repet; i++) {
    for(pas = 0; pas < maxpas; pas++) {
      if (sol1[numseq][pas] == 1) {
        digitalWrite(toy1, HIGH);
      }

      delay(50);
      digitalWrite(toy1, LOW);

      digitalWrite(ledPin, HIGH);
      delay (tempo/2);
      digitalWrite(ledPin, LOW);
      delay (tempo/2);
    }
  }
}



